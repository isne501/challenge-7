#include <iostream>
#include "strvar.h" //include stringvar header

using namespace std;
using namespace strvarken;

void conversation(int max_size); // function that stored conversation

int main()
{
	conversation(30); //Declare Max size of charactor is 30
	return 0; 
}
	
void conversation(int max_size)
{
	StringVar firstword(max_size), secondword(max_size); //for store the both word
	int select, nextcharacter;
	char character;

	cout << "Input your first-word : "; //Promt user input word one
	firstword.input_line(cin); //store word one to firstword 

	cout << "Input your second-word : "; //Promt user input word two
	secondword.input_line(cin); //store word one to secondword
	
	cout << endl;
	if (firstword == secondword) //Compare between two words
		cout << "Your both character are same!!" << endl; //If equal show this
	else
		cout << "Your both character aren't same!!" << endl; //If not equal show this

	StringVar word = firstword + secondword; //Sum word together
	cout << endl;
	cout << "The conversation is :  " << word << endl; //Show conversation
	
	cout << endl;
	cout << "Input the number of character you need to select (i.e. ABC if input '2' means B) : "; //Promt user select number of character
	cin >> select;
	
	cout << endl;
	cout << "Your character is : " << word.one_char(select) << endl; //Show character that user select
	
	cout << endl;
	cout << "Input the number of character you need to select and change (i.e. ABC if input '2 A' means AAC) : "; //Promt user select number of character and change
	cin >> select >> character;
	word.set_char(select, character);
	
	cout << endl;
	cout << "Your character is : " << word << endl; //Show conversastion after change a character
	
	cout << endl;
	cout << "Input the number of character you need to select and number of next character (i.e. ABCD if input '2 2' means BCD) : "; //Promt user select number of character and number of next charactor
	cin >> select >> nextcharacter;
	
	cout << "Your character is : "; 
	word.copy_piece(select, nextcharacter); //Show the character from select to nextcharacter
	cout << endl;

	cout << "Input word for using >> : "; //promt user to input word for using operator >>
	cin >> word;
	
	cout << endl;
	cout << "Your word is  : " << word; //Show the word
	cout << endl;

	system("pause");
}