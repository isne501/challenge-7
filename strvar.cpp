
//strvar.cpp

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"


using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];

		for (int i = 0; i < strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for (int i = 0; i < strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	//Operator + for sum word one and word two together
	StringVar operator+(StringVar &str1, StringVar &str2)
	{
		StringVar word;
		for (int i = 0; i < str1.max_length + 1; i++)
		{
			word.value[i] = str1.value[i]; //Set character from word one to word variable name
		}
		for (int i = str1.length() ; i < str2.max_length + 1  ; i++)
		{
			word.value[i] = str2.value[i- str1.length()]; //Set character from word two to word variable name
		}
		return word;
	}

	//Function that pick the character from user select
	char StringVar::one_char(int select)
	{
		return value[select - 1];
	}
	
	//Function that pick the character that user select and change to what charater user need
	void StringVar::set_char(int select,char input)
	{
		value[select - 1] = input;
	}

	//Compare betwween two words
	bool operator==(StringVar &str1, StringVar &str2) 
	{
		string wordone = str1.value;
		string wordtwo = str2.value;

		if (wordone == wordtwo)
			return true;
		else
			return false;
	}

	//function that show character from select to number of nextchar
	void StringVar::copy_piece(int select, int nextchar)
	{
		for (int i = select-1 ; i < ((select-1) + nextchar) + 1; i++)
		{
			cout << value[i];
		}
		cout << endl;
	}

	//Function that make program can use operator >> for store word to variable
	istream& operator >> (istream& input, const StringVar& str)
	{
		input >> str.value;
		return input;
	}
}//strvarken
